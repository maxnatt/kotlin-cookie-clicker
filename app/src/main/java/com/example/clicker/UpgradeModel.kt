package com.example.clicker

class UpgradeModel(val image: Int, var title: String,
                   val min_score_upgrade_1: Int, val score_upgrade_1: Int,
                   val min_score_upgrade_2: Int, val score_upgrade_2: Int,
                   val min_score_upgrade_3: Int, val score_upgrade_3: Int,
                   var curr_upgrade_lvl: Int, val upgradeName: String)