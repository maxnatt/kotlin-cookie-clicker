# Kotlin Cookie Clicker

Uni task to create a simple cookie clicker with upgrades/boost for Android using Kotlin, RecyclerView and SharedPreferences.

Upgrade/boost icons used withing the project belong to [payungkead](https://www.flaticon.com/authors/payungkead) and [freepik](https://www.flaticon.com/authors/freepik).
