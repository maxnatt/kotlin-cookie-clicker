package com.example.clicker

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder


class UpgradesAdapter(private val context: Context, private val items: ArrayList<UpgradeModel>,
                      private var upgradeClickInterface: UpgradeClickInterface
) :
    RecyclerView.Adapter<UpgradesAdapter.CustomViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        return CustomViewHolder(LayoutInflater.from(context).inflate(R.layout.upgrades, parent, false))
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        holder.bind(items[position])
        var minScore = ""
        when (items[position].curr_upgrade_lvl) {
            0 -> minScore = items[position].min_score_upgrade_1.toString()
            1 -> minScore = items[position].min_score_upgrade_2.toString()
            2 -> minScore = items[position].min_score_upgrade_3.toString()
            3 -> minScore = "Max"
        }
        holder.itemNeededScore.text = minScore
        holder.itemTitle.text = items[position].title
        holder.itemImage.setImageResource(items[position].image)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    inner class CustomViewHolder(view: View) : ViewHolder(view), View.OnClickListener {
        internal val itemNeededScore: TextView = view.findViewById(R.id.item_needed_score)
        internal val itemImage: ImageView = view.findViewById(R.id.item_image)
        internal val itemTitle: TextView = view.findViewById(R.id.item_title)
        private val upgradeLayout: RelativeLayout = view.findViewById(R.id.upgradeLayout)
        private var upgrade: UpgradeModel ?= null

        fun bind(upgrade: UpgradeModel) {
            this.upgrade = upgrade
            upgradeLayout.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            // Animate a click
            val img = v?.findViewById<ImageView>(R.id.item_image)
            img?.startAnimation(AnimationUtils.loadAnimation(v.context, R.anim.cookie_click))

            // Pass pressed upgrade to the GameActivity
            val newScore = upgradeClickInterface.upgradeClick(upgrade!!)

            // Update title
            val title = v?.findViewById<TextView>(R.id.item_title)
            title?.text = App.appResources?.getString(R.string.template_upgrade, upgrade!!.upgradeName, upgrade!!.curr_upgrade_lvl)

            // Update score
            if (newScore != "") {
                val score = v?.findViewById<TextView>(R.id.item_needed_score)
                score?.text = newScore
            }
        }
    }

    interface UpgradeClickInterface {
        fun upgradeClick(upgrade: UpgradeModel): String
    }

}

