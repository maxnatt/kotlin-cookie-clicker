package com.example.clicker

import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.Toast
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_game.*
import java.lang.reflect.Type
import java.util.*
import kotlin.collections.ArrayList
import kotlin.concurrent.fixedRateTimer
import kotlin.system.exitProcess

private const val COOKIE_BONUS_THRESHOLD_SEC = 5
private const val COOKIE_BONUS_VALUE = 100
private const val MAX_SCORE = 10L

class GameActivity : AppCompatActivity(), UpgradesAdapter.UpgradeClickInterface {
    private var currScore = 0L
    private var currCookiesSec = 0

    private var upgrades: ArrayList<UpgradeModel> = ArrayList()
    private val rand = Random()

    private var cookieBonusBannerShowing = false
    private var cookieBonusActive = false
    private var currCookieBonusSec = 1

    private var fadeInAnimation: Animation? = null
    private var fadeOutAnimation: Animation? = null

    private var globalTimer = 0L
    private var endGame = false

    override fun onCreate(savedInstanceState: Bundle?) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE)
        this.window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        val actionBar: ActionBar? = supportActionBar
        actionBar?.hide()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game)

        recycler_view.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)

        fixedRateTimer("timer", false, 0L, 1000) {
            timerTick()
        }

        fadeInAnimation = AnimationUtils.loadAnimation(this, R.anim.new_fadein)
        fadeOutAnimation = AnimationUtils.loadAnimation(this, R.anim.fadeout)
        bonusCookie.startAnimation(fadeOutAnimation)

        readSharedPrefs()

        updateCookiesSecLabel()
        updateScoreLabel()
    }

    private fun readSharedPrefs(){
        val pref = getSharedPreferences("save", MODE_PRIVATE)
        currScore = pref.getLong("currScore", 0L)
        globalTimer = pref.getLong("globalTimer", 0L)
        currCookiesSec = pref.getInt("currCookiesSec", 0)
        currCookieBonusSec = pref.getInt("currCookieBonusSec", 1)
        cookieBonusBannerShowing = pref.getBoolean("cookieBonusBannerShowing", false)
        cookieBonusActive = pref.getBoolean("cookieBonusActive", false)
        val gson = Gson()
        val json = pref.getString("upgrades", "")
        if (json == "") {
            val adapter = UpgradesAdapter(this, upgrades, this)
            recycler_view.adapter = adapter
            pushUpgrades(adapter)
        } else {
            val type: Type = object : TypeToken<ArrayList<UpgradeModel?>?>() {}.type
            upgrades = gson.fromJson(json, type)
            val adapter = UpgradesAdapter(this, upgrades, this)
            recycler_view.adapter = adapter
        }
    }

    private fun timerTick(){
        if (!endGame) {
            this.runOnUiThread {
                currScore += currCookiesSec
                updateScoreLabel()
                updateCookiesSecLabel()
                randomBoost()
                saveSharedPrefs()
                globalTimer++

                if (currScore >= MAX_SCORE) {
                    endGame = true
                    showWinnerDialog()
                }
            }
        }
    }

    private fun showWinnerDialog() {
        // Initialize a new instance of
        val builder = AlertDialog.Builder(this)
        // Set the alert dialog title
        builder.setTitle("Oh, look who won this game!")
        // Display a message on alert dialog
        builder.setMessage("It took just %s seconds of your life".format(globalTimer))
        // Set a positive button and its click listener on alert dialog
        builder.setPositiveButton("TAKE ME FROM HERE"){ _, _ ->
            exitProcess(0)
        }
        builder.setOnDismissListener(DialogInterface.OnDismissListener {
            exitProcess(0)
        })

        val dialog: AlertDialog = builder.create()
        dialog.show()
    }

    private fun randomBoost() {
        if (!cookieBonusActive) {
            if (!cookieBonusBannerShowing && rand.nextDouble() <= 0.08) {
                cookieBonusBannerShowing = true
                bonusCookie.startAnimation(fadeInAnimation)
                Handler().postDelayed({
                    bonusCookie.startAnimation(fadeOutAnimation)
                    cookieBonusBannerShowing = false
                }, 5000)
            }
        } else if (cookieBonusActive && currCookieBonusSec == COOKIE_BONUS_THRESHOLD_SEC) {
            cookieBonusActive = false
            currCookieBonusSec = 1
            currCookiesSec -= COOKIE_BONUS_VALUE
        } else {
            currCookieBonusSec++
        }
    }

    private fun pushUpgrades(adapter: UpgradesAdapter) {
        val initialLevel = 0
        upgrades.add(UpgradeModel(R.drawable.upgrade_glove, getString(R.string.glove_upgrade, initialLevel), 50, 1, 100, 2, 300, 3, initialLevel, "Glove"))
        upgrades.add(UpgradeModel(R.drawable.upgrade_rolling_pin, getString(R.string.pin_upgrade, initialLevel),  70, 2, 150, 2, 350, 3, 0, "Pin"))
        upgrades.add(UpgradeModel(R.drawable.upgrade_mixer, getString(R.string.mixer_upgrade, initialLevel), 400, 3, 600, 3, 800, 4, 0, "Mixer"))
        upgrades.add(UpgradeModel(R.drawable.upgrade_stove, getString(R.string.stove_upgrade, initialLevel), 1000, 1, 1500, 2, 2000, 3, 0, "Stove"))
        upgrades.add(UpgradeModel(R.drawable.upgrade_dishwasher, getString(R.string.dishwasher_upgrade, initialLevel), 3000, 1, 4000, 2, 5000, 3, 0, "Dishwasher"))
        upgrades.add(UpgradeModel(R.drawable.upgrade_chef, getString(R.string.chef_upgrade, initialLevel), 4500, 1, 8000, 2, 15000, 3, 0, "Chef"))
        adapter.notifyDataSetChanged()
    }

    fun cookieBonusClick(view: View) {
        if (cookieBonusBannerShowing) {
            bonusCookie.startAnimation(AnimationUtils.loadAnimation(this, R.anim.cookie_click))
            cookieBonusActive = true
            currCookiesSec += COOKIE_BONUS_VALUE
            saveSharedPrefs()
            updateCookiesSecLabel()
        }
    }

    /**
     * Processes user's click on the huge cookie.
     */
    fun cookieClick(view: View) {
        cookieBanner.startAnimation(AnimationUtils.loadAnimation(this, R.anim.cookie_click))
        currScore += 1
        saveSharedPrefs()
        updateScoreLabel()
    }

    private fun updateScoreLabel() {
        currScoreTextView.text = currScore.toString()
    }

    private fun updateCookiesSecLabel() {
        currCookiesSecTextView.text = (currCookiesSec).toString()
    }

    /**
     * Processes user's click on the upgrade.
     */
    override fun upgradeClick(upgrade: UpgradeModel): String {
        val (score_upgrade, min_score, next_score) = getUpgradePurchaseResult(upgrade)
        if (score_upgrade != -1) {
            currScore -= min_score
            upgrade.curr_upgrade_lvl++
            currCookiesSec += score_upgrade
            saveSharedPrefs()
            updateCookiesSecLabel()
            updateScoreLabel()
            Toast.makeText(this, "Upgrade was purchased", Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_SHORT).show()
        }
        return next_score
    }

    /**
     * Maps possible upgrade levels to needed minimum user's score.
     */
    private fun getUpgradePurchaseResult(upgradeModel: UpgradeModel): Triple<Int, Int, String> {
        var result = Triple(-1, -1, "")
        when (upgradeModel.curr_upgrade_lvl) {
            0 -> if (currScore >= upgradeModel.min_score_upgrade_1) result = Triple(upgradeModel.score_upgrade_1, upgradeModel.min_score_upgrade_1, upgradeModel.min_score_upgrade_2.toString())
            1 -> if (currScore >= upgradeModel.min_score_upgrade_2) result = Triple(upgradeModel.score_upgrade_2, upgradeModel.min_score_upgrade_2, upgradeModel.min_score_upgrade_3.toString())
            2 -> if (currScore >= upgradeModel.min_score_upgrade_3) result = Triple(upgradeModel.score_upgrade_3, upgradeModel.min_score_upgrade_3, "Max")
        }
        return result
    }

    private fun saveSharedPrefs(){
        val pref = getSharedPreferences("save", MODE_PRIVATE)
        val editor = pref.edit()
        val gson = Gson()
        editor.putLong("currScore", currScore)
        editor.putLong("globalTimer", globalTimer)
        editor.putInt("currCookiesSec", currCookiesSec)
        editor.putBoolean("cookieBonusBannerShowing", cookieBonusBannerShowing)
        editor.putBoolean("cookieBonusActive", cookieBonusActive)
        editor.putInt("currCookieBonusSec", currCookieBonusSec)
        val json = gson.toJson(upgrades)
        editor.putString("upgrades", json)
        editor.apply()
    }
}